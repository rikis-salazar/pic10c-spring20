# Syllabus: Winter 2020 Lecture 1

> If you are reading the online (html) version of this syllabus, [click
> here][pdf] to access a `.pdf` version of this document.[^if-you]

[pdf]: syllabus.pdf
[^if-you]: The embedded link does not work in the `.pdf` document.

---

## Course information

**Contact:** [`rsalazar@math.ucla.edu`][correo] (write "Pic 10C" in the
subject).

**Dates & Time:** Monday, Wednesday, Friday, from 11:00 to 11:50 am.

**Location:** Weather permitting we will alternate between _backyard, 
living room, and quite possibly a bedroom_.

**Office hours:** The following guidance has been provided by the math
department:

> "Instructors must offer two remote drop-in office hours per week at fixed
> times. **They should be scheduled in consultation with their students**
> (emphasis mine). In addition, instructors have to offer additional
> availability for students who cannot remotely attend the scheduled office
> hours."

In other words, I will set the office hours after I have _met_ with all of my
students during week 1 of instruction. Once the date/time has been set, an
official announcement will be sent to enrolled students, and it will be
published at [our CCLE class website (CCLE)][class-website].

**Teaching assistant(s):**

|**Section**| **T. A.**     |**Office**| **E-mail**                           |  
|:---------:|:--------------|:--------:|:-------------------------------------|  
| 1A        |Schuchardt, J.A.| `N/A`   | [`jasonsch@math.ucla.edu`][t-a]      |  

[correo]: mailto:rsalazar@math.ucla.edu
[CCLE-website]: https://ccle.ucla.edu/course/view/20S-COMPTNG10C-1
[t-a]: mailto:jasonsch@math.ucla.edu


## Course Description

_Miscellaneous topics in computer programming._ Roughly speaking, the course can
be broken into three different parts:

1.   `C++` topics.- the idea is to review/revisit and/or [re]introduce topics
     you might have studied in previous courses. These topics include, but are
     not limited to: _the big 3/4/5/6, inheritance, templates, [function]
     pointers, function objects, data structures, iterators, generic algorithms,
     binders & adapters, lambda closures, etc._
 
     > Since it is expected that some [or most] of you are already familiar with
     > a portion of these topics, the emphasis here will be placed on _how
     > things work under the hood_, as opposed to _how to use them in practice_.

1.   Tools for your final project.- creating/maintaining local/remote code
     repositories, _"visual" programming_ via the `Qt` framework.

     > We will simply _give you the basics_. It will be up to you to decide how
     > far into these topics you want to go.

1.   Developer tools in general.- _version control_ (`git`, `Markdown`), _build
     automation_ (`GNU Make`), and _markup file conversion_ (`pandoc`).


## Textbook

You are welcome to use your copy of Horstmann C. & Budd, T. A. Big `C++`, _2nd/3rd
Edition._ John Wiley and Sons, Inc. However, be advised that most of the
material presented during lecture can be found elsewhere. Whenever possible,
lecture notes and/or slides will be prepared, and if applicable, made available
to students.


## Course websites: PIC, CCLE and MyUCLA

During this quarter I will utilize three different websites:

*   [PIC (`www.pic.ucla.edu/~rsalazar/pic10c`)][pic]: This is to be considered
    the _main class website_. Examples, code, slides, assignment descriptions,
    etc., will be hosted here.
*   [CCLE][ccle]: for class announcements and homework submissions.
*   [MyUcla][myucla]: to report scores/grades.

[pic]: https://www.pic.ucla.edu/~rsalazar/spring2020/
[ccle]: https://ccle.ucla.edu/
[myucla]: https://my.ucla.edu/


## Reaching me via email

_Please consult CCLE announcements as well as this syllabus before sending me an
email about a policy or procedure as your question(s) might already be answered
here._

If you feel that your question/issue has not been addressed in the documents
listed above, do feel free to send me a message, however keep the following in
mind:

*   I receive a high volume of messages throughout the day: it might be faster
    for you to get the information you seek if you reach out to me _in person_
    via Zoom (say during O.H., or right before/after lecture).

*   Messages with special _keywords_ "skip" my inbox. Use this to your
    advantage: if you add `Pic 10c` to your subject line, your message will find
    its way into a special folder that I check periodically. This should reduce
    the time you have to wait before I reply to it.

*   Messages with special attachments, more specifically text files with
    specific file extensions (_e.g.,_ `.cpp`, `.h`, etc.), wind up in my _trash_
    folder.

    > This inconvenience brought to you by students (mostly from pic10a) that
    > think deadlines do not apply to them.


## Midterms

One fifty-minute midterm will be given on **May 4 (Monday Week 6)**. For the
time being they will be scheduled around 5 pm Pacific Time[^three].

The submission process will be detailed later and it might include a two-step
process:

1. Submission of a _low resolution_ set of pictures to CCLE within 10 minutes of
   the end of the exam; and
2. Submission of either a set of high resolution image(s), or a clearly legible
   _.pdf_ file to Gradescope. This process will have to be completed within 24
   hours of the end of the exam.

Exams will not be returned to you as you will not physically hand them over.
After receiving grading feedback, _any questions/concerns regarding how the
exam was graded must be submitted in writing_. You should be able to clearly
explain what mistake was made (if any), and why your argumentation is correct.


## Final Exam

This cumulative exam
will be _released_ on Wednesday, June 10 at 11:00 am Pacific Time. Unlike the
spring 2020 quarter, students **will not be given a 24-hr window to submit it.**
Instead students will be allowed to start the exam within 24 hrs of the time
described above, but once it is started, the submission process will have to be
completed within 3 and half hours. The submission process might also have to be
completed via two different platforms (_e.g.,_ CCLE and Gradescope).

> **Important:**  
>  
> _Failure to submit the final exam through the proper online platforms will
> result in an automatic F!_


## Grading

_Grading method:_ Assignments, Midterm Exam, Final Exam, and Final Project.

Your final score in the class will be the maximum of the following two grading
breakdowns:

|                                                                                    |
|:----------------------------------------------------------------------------------:|
| (15% Assignments) + (25% Midterm 1) + (30% Final Exam) + (35% Final Project)[^one] | 
| or                                                                                 |
| (15% Assignments) + (50% Final Exam) + (35% Final Project)                         |

[^one]: Please notice that the weights in this breakdown do not add up to 100%.

_Letter grades:_  

|                       |                      |                    |
|:----------------------|:---------------------|:-------------------|
|                       | A (93.33% or higher) | A- (90% -- 93.32%) |
| B+ (86.66% -- 89.99%) | B (83.33% -- 86.65%) | B- (80% -- 83.32%) |
| C+ (76.66% -- 79.99%) | C (73.33% -- 76.65%) | C- (70% -- 73.32%) |

The remaining grades will be assigned at my own discretion. Please DO NOT
request exceptions.

**All grades are final when filed by the instructor on the Final Grade Report.**


## Policies and procedures

Once a homework assignment is posted (CCLE) you will have at least two weeks to
complete it. The actual due date might change but it will always be available
in its corresponding CCLE assignment submission page. You are encouraged to
check CCLE on a regular basis to find out about changes. You will upload the
requested files before the deadline; failing to do so will result in your
submission receiving a _late_ mark.

There will be 4 or 5 homework assignments throughout the quarter. Please note
that since the number of assignments is low, and since their weight on your
final score is low as well, **your lowest homework score will not be dropped**.

_In an effort to be fair to all students, messages sent to my email address that
contain either `.cpp`, `.txt`, or  `.h` attachments will automatically be
deleted from my inbox._

You are responsible for naming your files correctly and you need to make sure
that you submit them through the proper channels. You are free to use your
favorite software but you should make an effort to test your code on multiple
different platforms if applicable. Keep in mind that the programming environment
I will use to _compile_ your projects might be considerably different from the
one you used to work on your project. For reference, the computer(s) I will use
during this quarter are equipped with
_Visual Studio 2017_,
as well as some recent version of
`g++`
and
`clang++`
(this latter compiler is the one that is used by _Xcode_). In addition to this
testing, you are highly encouraged to keep your source code (or at least the
code "living" in your _master branch_ of your repositories) in a clean state:
free of commented out blocks of code, no object files, nor IDE specific
configuration binary files, etc.

If  for some reason your code does not compile on any of the computers I have
available at the time I am grading, I will make my best effort to assign you a
fair score based on the code that I am able to review.

Scores on homework assignments and exams might appear on the CCLE website,
however, these scores will not be the official ones. Usually I ask the class
grader to partially grade some assignments after week 5, then upload these
grades to CCLE. At this point, if your score is less that 20/20, you should
attempt to fix it. **All of your assignments will be graded by myself after the
final exam.** These official scores as well as comments and/or observations
about your work will be reported to MyUCLA.


## About coding integrity

You are encouraged to discuss aspects of the course with other students as well
as homework assignments with others in general terms (_i.e.,_ general ideas and
words are OK but code is not OK). If you need specific help with your programs
you may consult any of the course TAs, or the course instructor. Copying major
parts of a program or document written by someone else (_e.g.,_ code found
online) should be avoided in general. Under certain circumstances --for example,
when starting a project from a template, or when trying to implement an
idea/algorithm presented to you during lecture/DS, and/or found online--,
copying is not _frowned upon_, but actually encouraged. If you believe you this
scenario applies to you, you should disclose the source/author of the code you
are including in your project. For example, you can write a comment along the
lines of

~~~~~ {.cpp}
// The following code is taken from www.some.web-site.edu/found-it/online
// where "Elmer H. Omero" uses it to solve the P = NP problem; which in turn
// can be relied on to solve the Riemann hypothesis.
~~~~~


## Final project

Your final project consists of a series of _'deliverables'_ starting at week 3.
You have 2 weeks to come up with an approved 6-week project. If by the end of
this time you have not settled on a project I will be happy to suggest one for
you. The idea behind this is to get you excited about the material. By now you
should have some expertise will likely be able to come up with a pretty cool
project. Just remember: _all projects need to be approved by me_.


## Accommodations

If you need any accommodations for a disability, please contact the UCLA
CAE[^two] [(`www.cae.ucla.edu`)][CAE]. Make sure to let me know as soon as
possible about necessary arrangements that need to be made.

[CAE]: http://www.cae.ucla.edu
[^two]: Center for Accessible Education
[^three]: I might be able to offer a 50 minute exam to be completed within a 24hr
  time frame akin to most winter 2020 finals. But I'm still looking into specif
  details about a possible implementation.

|                                                      |
|:----------------------------------------------------:|
| Course Syllabus Subject to Update by the Instructor. |

