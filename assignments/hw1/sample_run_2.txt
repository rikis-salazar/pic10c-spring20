You have $100. Enter bet: 100
Your cards:
	Dos de copas        (Two of cups).
Your total is 2. Do you want another card (y/n)? n
Dealer's cards:	Sota de espadas     (Jack of spades).
The dealer's total is 0.5.

New card:
	Rey de espadas (King of spades).

Dealer's cards:
	Sota de espadas     (Jack of spades).
	Rey de espadas      (King of spades).
The dealer's total is 1.

New card:
	Siete de bastos (Seven of clubs).

Dealer's cards:
	Siete de bastos     (Seven of clubs).
	Sota de espadas     (Jack of spades).
	Rey de espadas      (King of spades).
The dealer's total is 8.

You win 100.

You have $200. Enter bet: 1
Your cards:
	Caballo de bastos   (Knight of clubs).
Your total is 0.5. Do you want another card (y/n)? y
New card:
	As de espadas (Ace of spades).

Your cards:
	As de espadas       (Ace of spades).
	Caballo de bastos   (Knight of clubs).
Your total is 1.5. Do you want another card (y/n)? y
New card:
	Cuatro de espadas (Four of spades).

Your cards:
	As de espadas       (Ace of spades).
	Cuatro de espadas   (Four of spades).
	Caballo de bastos   (Knight of clubs).
Your total is 5.5. Do you want another card (y/n)? n
Dealer's cards:	Rey de bastos       (King of clubs).
The dealer's total is 0.5.

New card:
	Seis de bastos (Six of clubs).

Dealer's cards:
	Seis de bastos      (Six of clubs).
	Rey de bastos       (King of clubs).
The dealer's total is 6.5.

Too bad. You lose 1.

You have $199. Enter bet: 199
Your cards:
	Sota de copas       (Jack of cups).
Your total is 0.5. Do you want another card (y/n)? y
New card:
	Tres de bastos (Three of clubs).

Your cards:
	Tres de bastos      (Three of clubs).
	Sota de copas       (Jack of cups).
Your total is 3.5. Do you want another card (y/n)? y
New card:
	Cuatro de copas (Four of cups).

Your cards:
	Tres de bastos      (Three of clubs).
	Cuatro de copas     (Four of cups).
	Sota de copas       (Jack of cups).
Your total is 7.5. Do you want another card (y/n)? n
Dealer's cards:	Siete de copas      (Seven of cups).
The dealer's total is 7.

You win 199.

You have $398. Enter bet: 398
Your cards:
	Cinco de espadas    (Five of spades).
Your total is 5. Do you want another card (y/n)? n
Dealer's cards:	As de bastos        (Ace of clubs).
The dealer's total is 1.

New card:
	Siete de copas (Seven of cups).

Dealer's cards:
	As de bastos        (Ace of clubs).
	Siete de copas      (Seven of cups).
The dealer's total is 8.

You win 398.

You have $796. Enter bet: 796
Your cards:
	As de espadas       (Ace of spades).
Your total is 1. Do you want another card (y/n)? y
New card:
	Seis de copas (Six of cups).

Your cards:
	As de espadas       (Ace of spades).
	Seis de copas       (Six of cups).
Your total is 7. Do you want another card (y/n)? n
Dealer's cards:	Dos de oros         (Two of coins).
The dealer's total is 2.

New card:
	Sota de oros (Jack of coins).

Dealer's cards:
	Dos de oros         (Two of coins).
	Sota de oros        (Jack of coins).
The dealer's total is 2.5.

New card:
	Seis de oros (Six of coins).

Dealer's cards:
	Dos de oros         (Two of coins).
	Seis de oros        (Six of coins).
	Sota de oros        (Jack of coins).
The dealer's total is 8.5.

You win 796.

Congratulations. You beat the casino!

Bye!
