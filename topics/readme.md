# Course topics

Here you will find additional resources that complement the material we discuss
during lecture.

---

## Version control

We have toyed around with at least one local repository, as well as at least one
remote one (hosted on [bitbucket][my-bb]). While managing local repositories I
have mostly used a _command line interface_ (CLI for short), but have also
showcased some rudimentary _graphical user interfaces_ (GUI's for short).

Here is a list of resources you might want to consider checking out at some
point:

 *  [Bitbucket][BB] & [Github][GH]
 *  [Sourcetree][ST] & [Github Desktop][GHD]
 *  [Git tutorial (`github`)][tutorial-github]
 *  [Learn SourceTree with BitBucket Cloud (bitbucket)][LST-BBC]
 *  [GitHub Desktop User Guides][GHD-UG]
 *  [Hello World in GitHub][HW-GH]
 *  [An Intro to Git and GitHub for Beginners][IntroGit]

[my-bb]: https://bitbucket.org/rikis-salazar
[BB]: https://bitbucket.org/
[GH]: https://github.com/
[ST]: https://www.sourcetreeapp.com/
[GHD]: https://desktop.github.com/
[tutorial-github]: https://try.github.io/levels/1/challenges/1
[LST-BBC]: https://confluence.atlassian.com/bitbucket/tutorial-learn-sourcetree-with-bitbucket-cloud-760120235.html
[GHD-UG]: https://help.github.com/desktop/guides/
[HW-GH]: https://guides.github.com/activities/hello-world/
[IntroGit]: http://product.hubspot.com/blog/git-and-github-tutorial-for-beginners

> If you decide that GUI's are not for you, then I highly recommend you bookmark
> some of the following links:
>
>  *  [Git Tutorial (local branching on the cheap)][branch-cheap]. By the very
>     same developers of `git`.
>  *  [Git FAQ][git-faq]. For specific, everyday type of examples.
>  *  [Pro Git (book)][git-book]. In case you want to be a _Git-Fu_ master.
>     Also available in several downloadable formats:
>
>     - [PDF][book-pdf]
>     - [EPUB][book-epub]
>     - [MOBI][book-mobi]

[branch-cheap]: https://git-scm.com/docs/gittutorial
[git-faq]: http://gitfaq.org/
[git-book]: https://git-scm.com/book/en/v2
[book-pdf]: https://progit2.s3.amazonaws.com/en/2016-03-22-f3531/progit-en.1084.pdf
[book-epub]: https://progit2.s3.amazonaws.com/en/2016-03-22-f3531/progit-en.1084.epub
[book-mobi]: https://progit2.s3.amazonaws.com/en/2016-03-22-f3531/progit-en.1084.mobi

---

## Function pointers (the old C way)

> **Note:**
>
> I discussed this topic mostly as a way to motivate the introduction of
> _functors_ in C++ (see C++ STL Functions section below). Although I am willing
> to bet that most of you will never have to use a function pointer in your C++
> programing life; in my opinion it doesn't hurt to make an effort to understand
> how programming tools/concepts have evolved over the years.

In several instances during this course a common _theme_ will emerge, namely,
the perceived ability to _pass a function as a parameter to another function_ to
alter (read set the policy of) the receiving function. In this case, the passing
of the function is via a _plain-and-simple_ [pointer] variable[^note-psv]; this
pointer can be _de-referenced_ by adding a trailing set of parenthesis (that
may, or may not, enclose other variables or objects). The presence of this set
of parenthesis is what gives the impression that the _plain-and-simple_ variable is
_behaving like a function_. 


### Resources 

The examples discussed during lecture can be found [here][repo-fp]. In addition,
feel free to check out these other links:

*   [Programs as data: function pointers][fp-link1]
*   [Function pointer (wikipedia entry)][fp-link2]
*   [Function pointer examples][fp-link3]

[repo-fp]: https://bitbucket.org/rikis-salazar/10c-review-function-pointers
[fp-link1]: https://www.cprogramming.com/tutorial/function-pointers.html
[fp-link2]: https://en.wikipedia.org/wiki/Function_pointer
[fp-link3]: https://www.learncpp.com/cpp-tutorial/78-function-pointers/
[^note-psv]: Recall that roughly speaking a variable is an instance of a
  primitive type, whereas an object is an instance of a class.

---


## The C++ Standard Template Library (C++ STL)

### Containers

The idea here is to provide you with the information you need (_e.g.,_
requirements, complexity, etc.) to decide which particular container better suits your
needs; it should be easy then to locate appropriate documentation (:cough:
:cough: _Google it!_ :cough: :cough:), as well as examples of how said tools are
used in practice.

Material presented during lecture:

*   [The `c++` Standard Template Library (containers)][containers]

*   [Introduction to _Hash Tables_][pic10b-lesson12]

    > Note: this material is both _incomplete_ and _deprecated_; regardless, it
    > should give a good idea about the pros and cons of hash tables.

In addition, feel free to check out these other links.

_How they work_:

*   [A home-made non-template vector][10b-vector]: walks you through the inner
    workings of a vector container.
*   [The basics of a template doubly linked list][10b-lists]: details about
    the implementation of a list container.
*   [A home-made _stack_ and an abstract _queue_][10b-adaptors]: explains how
    adaptors reuse another container (as opposed to implement all functions from
    scratch).
*   [_Self-balancing binary search trees_][sbbst]: wikipedia entry containing
    both, general as well as technical info about these types of trees.
*   [_Hash tables_][ht]: wikipedia entry containing both, general as well as
    technical info about these types of trees.

_How they are used (examples)_

*   [`std::map` usage tutorial and example][m-example]: first part in series of
    iteresting postst that explain common tasks associated to maps.
*   [`std::unordered_map` usage tutorial and example][um-example]

> Both links above were the result of simple Google searches.


[10b-vector]: https://www.pic.ucla.edu/~rsalazar/pic10b/handouts/non-template-vector.html
[10b-lists]: https://www.pic.ucla.edu/~rsalazar/pic10b/handouts/template-list.html
[10b-adaptors]: https://www.pic.ucla.edu/~rsalazar/pic10b/handouts/container-adapters.html
[sbbst]: https://en.wikipedia.org/wiki/Self-balancing_binary_search_tree
[ht]: https://en.wikipedia.org/wiki/Hash_table
[containers]: https://bitbucket.org/rikis-salazar/10c-stl-containers
[pic10b-lesson12]: https://docs.google.com/presentation/d/e/2PACX-1vQvsJOFsP1iw8afT8PHoyRfrkC3HeH1-BaXJAF0HgxqH40lVvgpvIs0V0qKJwZfn983Hdy4rQVmPYLZ/pub?start=false&loop=false&delayms=3000
[um-example]: http://thispointer.com/unordered_map-usage-tutorial-and-example/
[m-example]: https://thispointer.com/stdmap-tutorial-part-1-usage-detail-with-examples/


### Functions (function objects)

A functor (or function object) in C++ is an instance (hence the object name) of
a class (say `SomeClass`) that provides at least one definition of the member
function `operator()`. This allows the object (say `some_object`) to make a
call to the parenthesis operator. This call can look like this:
`some_object.operator(...)`; but it typically looks more like this:
`some_object(...)`; which gives the impression that it is actually a function.

Notice that any regular function (say `some_regular_function`) that takes an
object of the class `SomeClass` as a parameter will fall into this category of
_functions that take functions as parameters_.

### Resources 

The examples discussed during lecture can be found [here][repo-fo]. In addition,
feel free to check out these other links:

*   [Functors in C++: examples][fo-link1]
*   [Function object (wikipedia entry)][fo-link2]

[repo-fo]: https://bitbucket.org/rikis-salazar/10c-templates
[fo-link1]: https://www.geeksforgeeks.org/functors-in-cpp/
[fo-link2]: https://en.wikipedia.org/wiki/Function_object

---


[Return to main course website][PIC]

[PIC]: ..
