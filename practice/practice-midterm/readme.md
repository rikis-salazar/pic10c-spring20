## Practice problems for Midterm 1

1.  True or False  

    -   Template classes are not allowed to have default template parameters.
    -   The statement `void* (*f)(void*,void*);` is a valid declaration of a
        function pointer.
    -   The `Qt` application framework does not allow you to write `C++` code
        with memory leaks as long your _main window_ is requested from the
        _Stack_.
    -   It is not possible to inherit from a purely virtual class.
    -   Garbage collection is not available in C++.


1.  Describe the advantage (or advantages) of version control software (e.g.,
    _git, mercurial, subversion, etc_.) over services like _dropbox_ and/or
    _google drive_. 


1.  Describe the process to setup and update a git repository.


1.  Very likely in a previous C++ class you were taught to implement the
    assignment operator following the steps below

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    SomeClass& SomeClass::operator=( const SomClass& rhs ){
        // 1. Check for self-assignment 
        if ( this != &rhs ) {

            // 2. Release resources associated to implicit param
            release_resources(); // <-- delete or delete[] called here

            // 3. Request heap memory and transfer data
            some_type* some_ptr = new SomeConstructor();
            this->perform_deep_copy(rhs);
        }

        // 4. Return implicit object
        return *this;
       }
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

    On the other hand, in this class you were told that the code

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    SomeClass& SomeClass::operator=( SomClass rhs ){
        some_function_that_correctly_swaps(*this,rhs);
        return *this;
    }
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

    has the same effect. Briefly discuss the advantages of one over the other.


1.  Consider the template function `find_smallest` defined by

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    #include <stdexcept>       // <-- std::invalid_argument
    #include <vector>

    template <typename T>
    T find_smallest( const std::vector<T>& v ){

        if ( v.size() == 0 )
            throw std::invalid_argument("Empty vector.");

        T min = v[0];
        for ( auto& x : v )    //  <-- See (*) below
            if ( x < min )
                min = x;

        return min;
    }
    // (*) In case you have not seen this type of `for` statement before, it is
    // equivalent to
    //
    //     for ( int i = 0 ; i < v.size() ; i++ ) 
    //         if ( v[i] < min )
    //             min = v[i];
    //
    // As you can see, `x` above plays the role of the value at _position_ `i`.
    // The `&` is there to indicate that `x` is _passed by_ reference (as
    // opposed to _by value_).
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

    i.  Which is the minimum set of assumptions that are being placed on the
        type `T`? In other words, which are the functions and/or operators that
        need to be well defined in order for the code above to compile?

    i.  Add a second template parameter `CMP` to `find_smallest` so that the
        user can provide a comparator function object.  If no function object is
        provided have the function use the comparator `std::less<T>` in
        `<functional>`.
---

[Return to main course website][PIC]

[PIC]: ../..

