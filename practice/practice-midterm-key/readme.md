## Practice problems for Midterm 1

1.  True or False  

    -   Template classes are not allowed to have default template parameter
        values.

        > **False.** They behave like regular function parameters.

    -   The statement `void* (*f)(void*,void*);` is a valid declaration of a
        function pointer.

        > **True.** It declares a pointer to a function that receives two
        > _generic_ pointers as parameters and returns a _generic_ pointer by
        > value.

    -   The `Qt` application framework does not allow you to write `C++` code
        with memory leaks as long your _main window_ is requested from the
        _Stack_.

        > **False.** It is still possible to write code that leaks if the _RAII_
        > guidelines discussed during class are not followed.  For example, if
        > all `Qt`-objects are requested from the heap; or if a heap object is
        > not properly transfered to a stack object.

    -   It is not possible to inherit from a purely virtual class.

        > **False.** It is possible to inherit from such class. However, if not
        > all of the purely virtual members of the base class are overloaded, it
        > is not possible to instantiate (_i.e.,_ create objects belonging to)
        > the derived class. In other words, the derived class is also virtual.

    -   Garbage collection is not available in C++.

        > **True**  
        >
        > Joke:  
        > Q: _How can you tell someone is a c++ programmer?_  
        > A: They think garbage collection is a weekly sanitation service.


1.  Describe the advantage (or advantages) of version control software (e.g.,
    _git, mercurial, subversion, etc_.) over services like _dropbox_ and/or
    _google drive_. 

    > _Google Drive_ as well as _Dropbox_ provide an offsite backup of files. In
    > the event of a system crash the files can be recovered easily. One
    > advantage is that said files are easily available through a web interface.
    > This makes it very easy for the user to access/edit files and keep them in
    > sync across several devices. However, the user is not directly in control
    > of when changes are recorded. Moreover if two users share a common
    > account, the risk of data corruption increases.
    >
    > Version control software (e.g., _Git_) by itself does not provide an
    > offsite backup. However, it can be combined with hosts such as _Github_,
    > and _Bitbucket_.  In this case, the user/programmer is in complete control
    > of the sync process. Moreover, it is designed to handle multiple entities
    > editing and/or adding files at different times. In case of conflicts,
    > changes are not forced and the user(s) have an opportunity to resolve
    > them.

1.  Describe the process to setup and update a git repository.

    > The process essentially boils down to
    >
    > > i.  Setup a repository via one [or more] of the following commands:
    > >     `git clone`, `git pull`, `git fetch`, or `git init`.
    > > i.  Edit/modify content from the repository.
    > > i.  Stage file(s) via `git add`.
    > > i.  Commit changes (_i.e.,_ take a _snapshot_ of the current state of
    > >     the repo) via `git commit`.

    >
    > Other steps include but are not limited to:
    >
    > > *   Create/delete branches
    > > *   Add/remove tags
    > > *   Move between commits
    > > *   Merging branches
    > > *   Reverting to previous versions of file(s)

1.  Very likely in a previous C++ class you were taught to implement the
    assignment operator following the steps below

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    SomeClass& SomeClass::operator=( const SomClass& rhs ){
        // 1. Check for sel-assignment 
        if ( this != &rhs ) {

            // 2. Release resources associated to implicit param
            release_resources(); // <-- delete or delete[] called here

            // 3. Request heap memory and transfer data
            some_type* some_ptr = new SomeConstructor();
            this->perform_deep_copy(rhs);
        }

        // 4. return implicit object
        Return *this;
       }
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

    On the other hand, in this class you were told that the code

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    SomeClass& SomeClass::operator=( SomClass rhs ){
        some_function_that_correctly_swaps(*this,rhs);
        return *this;
    }
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

    has the same effect. Briefly discuss the advantages of one over the other.

    > Both versions perform the same task if there is no self-assignment.
    > However because of the check `if ( this != &rhs )` in the first version,
    > the second version might actually be slower (depending on whether the
    > explicit parameter is _copied_ or _moved_).
    >
    > Other than that, the first version is easier to comprehend but, since it
    > involves writing more code, the likelihood of introducing an error
    > increases. The opposite is true for the second version: shorter and easier
    > to maintain, but harder to understand.

1.  Consider the template function `find_smallest` defined by

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    #include <stdexcept>       // <-- std::invalid_argument
    #include <vector>

    template <typename T>
    T find_smallest( const std::vector<T>& v ){

        if ( v.size() == 0 )
            throw std::invalid_argument("Empty vector.");

        T min = v[0];
        for ( auto& x : v )    //  <-- See (*) below
            if ( x < min )
                min = x;

        return min;
    }
    // (*) In case you have not seen this type of `for` statement before, it is
    // equivalent to
    //
    //     for ( int i = 0 ; i < v.size() ; i++ ) 
    //         if ( v[i] < min )
    //             min = v[i];
    //
    // As you can see, `x` above plays the role of the value at _position_ `i`.
    // The `&` is there to indicate that `x` is _passed by_ reference (as
    // opposed to _by value_).
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

    i.  Which is the minimum set of assumptions that are being placed on the
        type `T`? In other words, which are the functions and/or operators that
        need to be well defined in order for the code above to compile?

        > The function assumes that  `operator<`, as well as either the copy
        > constructor or the move constructor are well defined for type `T`.

    i.  Add a second template parameter `CMP` to `find_smallest` so that the
        user can provide a comparator function object.  If no function object is
        provided have the function use the comparator `std::less<T>` in
        `<functional>`.

        > ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
        > template < typename T, typename CMP = std::less< T > >
        > T find_smallest( const std::vector<T>& v ){
        >
        >     if ( v.size() == 0 )
        >         throw std::invalid_argument("Empty vector.");
        >
        >     T min = v[0];
        >     for ( auto& x : v )
        >         if ( CMP()(x,min) )  // <-- See note below!
        >              min = x;
        >
        >     return min;
        > }
        > ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
        > 
        > **Note:**
        >
        >  If instead of  `CMP()(x,min)` we write  `CMP(x,min)`, the compiler
        >  attempts to call a constructor that takes two parameters instead. It
        >  should then issue an error along the lines of
        >  `" ... no matching function for call to std::less< T >::less(T&, T&)..."`
        >  This is because the template parameter `CMP` refers to the function
        >  object rather than the member function `operator()` that is used to
        >  determine the _smallest_ value in the vector. Moreover, by the time
        >  the comparison is performed, the `CMP` class has not been
        >  instantiated. In contrast, the statement  `CMP()(x,min)`  first
        >  instantiates the class (i.e., constructs a [temporary] object), and
        >  later makes a call to the member function `operator()` to perform the
        >  comparison.
        >
        >  An alternative solution calls for the addition of a **regular**
        >  parameter to perform the instantiation.
        >
        > ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
        > template < typename T, typename CMP = std::less< T > >
        > T find_smallest( const std::vector<T>& v, CMP is_less = CMP() ){
        >     // ...
        >
        >     // if ( CMP()(x,min) )  // No longer needed, but it still works
        >
        >     if ( is_less(x,y) )
        >
        >    // ...
        > }
        > ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
        >
        > Yet, one more alternative solution calls for instantiation of `CMP`
        > within the function.
        >
        > ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
        > template < typename T, typename CMP = std::less< T > >
        > T find_smallest( const std::vector<T>& v ){
        >     CMP is_less;  // <-- same as:    CMP is_less = CMP();
        >     // ...
        > }
        > ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 
        > This later approach is the one in one of the Bitbucket repositories.
        > Look for the instantiation `CMP is_less;` in the private fields of the
        > `pair` example.

---

[Return to main course website][PIC]

[PIC]: ../..

