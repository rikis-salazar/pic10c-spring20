## Practice problems for Final Exam

1.  True or False

    -   All iterators are _multi pass_ iterators; that is, they are allowed to
        access elements in a container as many times as they like.
    -   A _random access iterator_ is guaranteed to have both versions (pre-fix
        and post-fix) of the `--operator()` member function.
    -   _Generic algorithms_ can only be used with either _pointers_, or with
        iterators from standard containers.
    -   Pointers can be used as iterators in any function in the `<algorithm>`
        standard library.
    -   A _lambda function_ can only be used within the `main()` function of a
        program.


1.  What does **RAII** stand for? Briefly explain how it prevents memory leaks
    when [this concept is] carefully used.


1.  Without using any `for` or `while` loops, implement the function
    `make_partition` with signature

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    std::vector<double> make_partition( double a, double b, int n );
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

    that returns either an empty vector (_if $n$ is not positive_), or a vector
    containing the $n+1$ values

    $$\{a, a+\Delta, a+2\Delta, \dots , a + n\Delta \},
    \quad\text{where}\quad
    \Delta = \frac{b-a}{n}.$$

    For example, `make_partition(1,3,4)` returns a vector with values
    `{1,1.5,2,2.5,3}`.


1.  Complete the code below, so that the `to_output_ostream` function _sends_
    the values in `[first,last)` to either the `ostream` object passed as third
    parameter, or to `std::cout` if the parameter is omitted.

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    template <typename              >
    std::ostream& to_output_stream(                    first,
                                                       last,
                                    std::ostream& out =                ){

        while (                      ) {



        }

        out << '\n';
        return out;
    }
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 


1.  Consider the incomplete interface file `matrix.h`

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    #ifndef MATRIX_H
    #define MATRIX_H

    class Matrix{
        private:
            double *theData;
            size_t rows;
            sixe_t cols;

        public:
            // Missing functions
    };

    #endif
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

    as well as the `matrix-test.cpp` file below

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    #include <iostream>
    #include "matrix.h"

    int main(){
        Matrix A(3,2);

        A(0,0) = 1;         A(0,1) = 4;
        A(1,0) = 2;         A(1,1) = 5;
        A(2,0) = 3;         A(2,1) = 6;

        // Displays 123
        std::cout << A(0,0) << A(1,0) << A(2,0) << "\n";

        return 0;
    }
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

    Provide the declaration and definition of the minimum set of functions
    needed so that the combination of files above compiles and displays the
    number 123 when the resulting program is run.

---

[Return to main course website][PIC]

[PIC]: ../..

