## Practice problems for Final Exam

1.  True or False

    -   All iterators are _multi pass_ iterators; that is, they are allowed to
        access elements in a container as many times as they like.

        > **False**

    -   A _random access iterator_ is guaranteed to have both versions (pre-fix
        and post-fix) of the `--operator()` member function.

        > **True**

    -   _Generic algorithms_ can only be used with either _pointers_, or with
        iterators from standard containers.

        > **False** _Home-made_ (or _user defined_) iterators can also be used
        > provided they _have_ the correct set of iterator traits.

    -   Pointers can be used as iterators in any function in the `<algorithm>`
        standard library.

        > **True** Pointers provide all functions/operators expected for any
        > type of iterator.

    -   A _lambda function_ can only be used within the `main()` function of a
        program.

        > **False**


1.  What does **RAII** stand for? Briefly explain how it prevents memory leaks
    when [this concept is] carefully used.

    > It stands for **R**esource **A**cquisition **I**s **I**nitialization. In
    > short, the idea is to represent a resource [e.g., file(s), dynamic
    > memory,etc] by a local object, so that the local object's destructor
    > releases the resource when it goes out of scope.

1.  Without using any `for` or `while` loops, implement the function
    `make_partition` with signature

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    std::vector<double> make_partition( double a, double b, int n );
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

    that returns either an empty vector (_if $n$ is not positive_), or a vector
    containing the $n+1$ values

    $$\{a, a+\Delta, a+2\Delta, \dots , a + n\Delta \},
    \quad\text{where}\quad
    \Delta = \frac{b-a}{n}.$$

    For example, `make_partition(1,3,4)` returns a vector with values
    `{1,1.5,2,2.5,3}`.

    > The _trick_ is to use the generic algorithm `for_each`, as well as a
    > lambda function:
    >
    > ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    > std::vector<double> make_partition( double a, double b, int n ) {
    >     std::vector<double> v;  // <-- Empty vector.
    >
    >     if ( n > 0 ) {
    >         // One resize to rule them all...
    >         v.resize(n+1);
    >
    >         // Capture by reference since it will be increased
    >         int i = 0 ;
    >
    >         // Capture by value to avoid changing it
    >         double delta = ( b - a ) / n ;
    >
    >         for_each( v.begin(), v.end(), 
    >                   // capture by value... except for 'i'
    >                   [=,&i](double& x){ x = a + i++ * delta; } );
    >
    >     }
    >
    >     return v;
    > }
    > ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


1.  Complete the code below, so that the `to_output_ostream` function _sends_
    the values in `[first,last)` to either the `ostream` object passed as third
    parameter, or to `std::cout` if the parameter is omitted.

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    template <typename              >
    std::ostream& to_output_stream(                    first,
                                                       last,
                                    std::ostream& out =                ){

        while (                      ) {



        }

        out << '\n';
        return out;
    }
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

    > The things that are missing here are: the template parameter, the default
    > parameter, and the body of the `while` loop. The template parameter can be
    > generic [e.g., `T`], or descriptive; I will use `InputIterator` to
    > indicate the functionality we expect from the parameter. The default
    > parameter is `std::cout`, and the body of the loop is pretty simple:
    > output the value, and increment the iterator.
    >
    > ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    > template <typename InputIterator>
    > std::ostream& to_output_stream( InputIterator first,
    >                                 InputIterator last, 
    >                                 std::ostream& out = std::cout ){
    >
    >     while ( first != last )
    >         out << *first++ << ',' ;  // <-- ',' is not needed
    >
    >     out << '\n';
    >     return out;
    > }
    > ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

1.  Consider the incomplete interface file `matrix.h`

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    #ifndef MATRIX_H
    #define MATRIX_H

    class Matrix{
        private:
            double *theData;
            size_t rows;
            sixe_t cols;

        public:
            // Missing functions
    };

    #endif
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

    as well as the `matrix-test.cpp` file below

    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    #include <iostream>
    #include "matrix.h"

    int main(){
        Matrix A(3,2);

        A(0,0) = 1;         A(0,1) = 4;
        A(1,0) = 2;         A(1,1) = 5;
        A(2,0) = 3;         A(2,1) = 6;

        // Displays 123
        std::cout << A(0,0) << A(1,0) << A(2,0) << "\n";

        return 0;
    }
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

    Provide the declaration and definition of the minimum set of functions
    needed so that the combination of files above compiles and displays the
    number 123 when the resulting program is run.

    > The minimun set of functions needed are: a two-parameter constructor, a
    > [virtual] destructor, and an overload of `operator()`.
    >
    > Here the trick is to use a dynamic array with `rows * cols` entries to
    > store the full Matrix. To access the stored elements we simply need to
    > calculate their index. In this case, element `M(i,j)` can be found at
    > position `i*cols +j`.
    >
    > ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ {.cpp}
    > #ifndef MATRIX_H
    > #define MATRIX_H
    >
    > class Matrix{
    >     private:
    >         double *theData;
    >         size_t rows;
    >         sixe_t cols;
    >
    >     public:
    >         Matrix(size_t r, size_t c) 
    >           : theData( new double[r*c] ), rows(r), cols(c) { }
    >
    >         virtual ~Matrix() { delete[] theData; }
    >
    >         double& operator(size_t i, size_t j){
    >             return theData[ i*cols + j ];
    >         }
    >
    >         // Not needed. Why not?
    >         // const double& operator(size_t i, size_t j) const {
    >         //     return theData[ i*cols + j ];
    >         // }
    > };
    > ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ 

---

[Return to main course website][PIC]

[PIC]: ../..

