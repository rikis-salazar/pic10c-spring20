# PIC 10C: Advanced Programming (winter 2020)

In previous quarters, I have used combination of resources: [course CCLE
website][CCLE], as well as a [personal _Bitbucket_ account][BitBucket]; to host
material related to this course.  The setup has worked _well enough_ but
students have reported that it is, at times, hard to locate material (_e.g.,_
code, handouts, etc.).

The purpose of having [_yet another class website_][PIC] is to make content more
accessible to everyone. This site should _point you to the right places_.
Whether you want to find a specific [code repository][repos], or [assignment
descriptions][hw]; they all should be _reachable_ from this location.

For the most part things should _just work_; however, if you encounter an issue
(_e,g,_ missing material, broken links, etc.) let us know so that we can fix it
as soon as possible.


---

## Class contents

 *  [Syllabus][syll-html] ([PDF][syll-pdf], [Markdown _raw_][syll-raw-md])

 *  [Topics and resources][topics]

 *  [Lecture summaries][lectures]

 *  [Bitbucket repositories][repos] 

 *  [Homework assignments][hw] 

 *  [Practice material][prac] 

 *  [Lecture recordings][rec]

---

## Course websites

During this transition period, while moving away from CCLE, material will be
hosted mostly at the _laguna_ (PIC) math server. On occasions you might still be
referred back to CCLE and/or other external sites. Here is a list of places you
might want to keep _within reach_ (_e.g._ internet bookmarks),

 *  [The CCLE website][CCLE]:

    > Use it to _submit_ your assignments, read class announcements, locate
    > information about O.H., etc.

 *  [The PIC website][PIC]:

    > For _slides_, `code`, and other resources.

 *  [The `pic10c-spring20` repository][BB-W20]:

    > A mirror of the PIC website. It might be a little harder to navigate, but
    > you get access to the inner workings of this particular setup.

Also, at times you might be referred to other bitbucket repositories. Most of
them can be located via [this link][BitBucket]. Use the search feature (_e.g.,_
containers, RAII, smart pointers, etc.) to quickly locate particular
repositories.

> **Note:** Some of the repos hosted therein are _deprecated_. In other words,
> I no longer maintain them, and they might contain mistakes ranging from
> insignificant typos, to complete blunders.


[CCLE]: https://ccle.ucla.edu/course/view/20S-COMPTNG10C-1
[BitBucket]: https://bitbucket.org/rikis-salazar/
[PIC]: https://www.pic.ucla.edu/~rsalazar/spring2020/
[BB-W20]: https://bitbucket.org/rikis-salazar/pic10c-spring20

[syll-html]: syllabus/
[syll-pdf]: syllabus/syllabus.pdf
[syll-raw-md]: syllabus/syllabus.txt

[topics]: topics/
[lectures]: lectures/
[hw]: assignments/
[prac]: practice/

[repos]: repos/
[rec]: https://ccle.ucla.edu/course/view.php?id=86868&section=3

<!-- This is a comment  
[handouts]: https://www.kualo.co.uk/404
-->
